from django.db import models

# Create your models here.
class Category(models.Model):
	name = models.CharField(max_length=32, verbose_name="نام دسته")
	slug = models.SlugField(unique=True, verbose_name="آدرس دسته")
	position = models.IntegerField()

	def __str__(self):
		return self.name

	class Meta:
		verbose_name = "دسته بندی"
		verbose_name_plural = "دسته بندی ها"
		ordering = ('id',)