from django.urls import path
from .views import home, category, article

app_name = 'article'

urlpatterns = [
    path('', home, name="home"),
    path('category/<str:slug>', category, name="category"),
    path('article/<str:slug>', article, name="article"),
]