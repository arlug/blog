from django.db import models
from django.contrib.auth.models import User
from category.models import Category
# from django.utils import timezone
from datetime import datetime
import time

# Create your models here.
class Article(models.Model):
	title = models.CharField(max_length=250)
	slug = models.SlugField(unique=True)
	summary = models.TextField(max_length=500)
	content = models.TextField()
	image = models.ImageField(null=True, blank=True, upload_to=time.strftime("%Y/%m/%d"))
	category = models.ForeignKey(Category, on_delete=models.SET_NULL, null=True, blank=True)
	author = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True)
	publish = models.DateTimeField(default=datetime.now())
	created = models.DateTimeField(auto_now=True)
	updated = models.DateTimeField(auto_now_add=True)

	def __str__(self):
		return self.title

	def fullname(self):
		return "{} {}".format(self.author.first_name, self.author.last_name)

	class Meta:
		verbose_name = "مقاله"
		verbose_name_plural = "مقالات"
		ordering = ('-publish',)