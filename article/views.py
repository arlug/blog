from django.shortcuts import render, get_object_or_404
from category.models import Category
from article.models import Article
from django.http import JsonResponse

# Create your views here.
def home(request):
	categories = Category.objects.all()
	articles = Article.objects.all()
	context = {
		'categories': categories,
		'articles': articles,
	}
	return render(request, 'article/home.html', context)

def article(request, slug):
	article = get_object_or_404(Article, slug=slug)
	categories = Category.objects.all()
	context = {
		'categories': categories,
		'article': article,
	}
	return render(request, 'article/article.html', context)

def category(request, slug):
	categories = Category.objects.all()
	category = get_object_or_404(Category, slug=slug)
	articles = category.article_set.all()
	context = {
		'categories': categories,
		'category': category,
		'articles': articles
	}
	return render(request, 'article/category.html', context)